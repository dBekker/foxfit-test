import 'package:flutter/material.dart';
import 'package:myapp/screens/statistics/widgets/sales.dart';
import 'package:myapp/screens/statistics/widgets/servises.dart';
import 'package:myapp/screens/statistics/widgets/tabs.dart';
import 'package:myapp/utils.dart';

class StatisticsScreen extends StatelessWidget {
  const StatisticsScreen({super.key});

  @override
  Widget build(BuildContext context) {

    return Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            SizedBox(height: 20),
            TabBarMounths(),
            SizedBox(height: 20),
            SalesRow(),
            SizedBox(height: 40),
            Align(
              alignment: Alignment.centerLeft,
                child: Text(
                  'Рейтинг',
                  style: SafeGoogleFont (
                    'Open Sans',
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    height: 1,
                    color: Color(0xff333e63),
                  ),
                )
            ),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                  color: Color(0xffffffff),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xfff3f3f3),
                      offset: Offset(0, 4),
                      blurRadius: 10,
                    ),
                  ]
              ),
              child: Column(
                children: [
                  Card(
                      clipBehavior: Clip.hardEdge,
                      shadowColor: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        child:  ListTile(
                          title: Row(
                            children: [
                              Text(
                                'По продажам: ',
                                style: SafeGoogleFont (
                                  'Open Sans',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  height: 1.5,
                                  color: Color(0xff3a4060),
                                ),
                              ),
                              Text(
                              '26 место ',
                                style: SafeGoogleFont (
                                  'Open Sans',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  height: 1.5,
                                  color: Color(0xff3a4060),
                                )
                              )
                            ],
                          ),
                          subtitle: Text('Нужно 14 000 ₽ до 25 места'),
                          trailing: Icon(Icons.chevron_right),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -2),
                        )
                      )
                  ),
                  Divider(
                      indent: 15.0,
                      endIndent: 15.0,
                  ),
                  Card(
                      clipBehavior: Clip.hardEdge,
                      shadowColor: Colors.transparent,
                      child: InkWell(
                          onTap: () {},
                          child:  ListTile(
                            title: Row(
                              children: [
                                Text(
                                  'По реализации: ',
                                  style: SafeGoogleFont (
                                    'Open Sans',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    height: 1.5,
                                    color: Color(0xff3a4060),
                                  ),
                                ),
                                Text(
                                    '25 место ',
                                    style: SafeGoogleFont (
                                      'Open Sans',
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      height: 1.5,
                                      color: Color(0xff3a4060),
                                    )
                                )
                              ],
                            ),
                            subtitle: Text('Нужно 18 000 ₽ до 24 места'),
                            trailing: Icon(Icons.chevron_right),
                            dense: true,
                            visualDensity: VisualDensity(vertical: -2),
                          )
                      )
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            TabBarServices()
          ],
        ),
    );
  }
}