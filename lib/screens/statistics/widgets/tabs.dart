import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'package:myapp/utils.dart';

class TabBarMounths extends StatefulWidget {
  const TabBarMounths({super.key});

  @override
  State<TabBarMounths> createState() => _TabBarMounthsState();
}

class _TabBarMounthsState extends State<TabBarMounths>
  with TickerProviderStateMixin {
  final controller = GroupButtonController(selectedIndex: 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GroupButton(
        controller: controller,
        buttons: ['Декабрь', 'Январь', 'Февраль'],
        options: GroupButtonOptions(
          selectedTextStyle: SafeGoogleFont (
            'Open Sans',
            fontSize: 14,
            fontWeight: FontWeight.w400,
            height: 1.4,
            color: Color(0xfffafafa),
          ),
          unselectedColor: Color(0xfffafafa),
          unselectedTextStyle: SafeGoogleFont (
            'Open Sans',
            fontSize: 14,
            fontWeight: FontWeight.w400,
            height: 1.4,
            color: Color(0xff2988e1),
          ),
          borderRadius: BorderRadius.circular(8),
          buttonWidth: 110.0
        )
    );
  }
}