import 'package:flutter/material.dart';
import 'package:myapp/utils.dart';

class SalesRow extends StatelessWidget {
  const SalesRow({super.key});

  @override
  Widget build(BuildContext context) {
    double baseWidth = 375;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SizedBox(
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            // autogroupxzek8hH (AK6D8KAWaUAGfCJ7tExZEK)
            margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 8*fem, 1*fem),
            padding: EdgeInsets.fromLTRB(11*fem, 14*fem, 11*fem, 11*fem),
            width: 163*fem,
            decoration: BoxDecoration (
              borderRadius: BorderRadius.circular(15*fem),
              gradient: const LinearGradient (
                begin: Alignment(-0.896, 0.931),
                end: Alignment(1, -0.725),
                colors: <Color>[Color(0xff3c4262), Color(0xff4d547b)],
                stops: <double>[0, 1],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // walletaddwud (I4901:21698;3179:168953)
                  margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 8*fem),
                  width: 24*fem,
                  height: 24*fem,
                  child: Image.asset(
                    'assets/images/wallet-add.png',
                    width: 24*fem,
                    height: 24*fem,
                  ),
                ),
                Container(
                  // autogroupw6v9eJF (AK6DE4VwA8cNbgX8anW6V9)
                  width: 111*fem,
                  height: 45*fem,
                  child: Stack(
                    children: [
                      Positioned(
                        // xpj (I4901:21698;3179:168949)
                        left: 0*fem,
                        top: 0*fem,
                        child: Align(
                          child: SizedBox(
                            width: 111*fem,
                            height: 24*fem,
                            child: Text(
                              'Продажи : 34 шт',
                              style: SafeGoogleFont (
                                'Open Sans',
                                fontSize: 13*ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.8461538462*ffem/fem,
                                color: const Color(0xffffffff),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        // dfy (I4901:21698;3179:168951)
                        left: 0*fem,
                        top: 21*fem,
                        child: Align(
                          child: SizedBox(
                            width: 82*fem,
                            height: 24*fem,
                            child: Text(
                              '156 000 ₽',
                              style: SafeGoogleFont (
                                'Open Sans',
                                fontSize: 18*ffem,
                                fontWeight: FontWeight.w700,
                                height: 1.3333333333*ffem/fem,
                                color: const Color(0xfffafafa),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            // autogroupwpvwJXD (AK6DLUUv1TywHUJkfgWpvw)
            margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 0*fem),
            padding: EdgeInsets.fromLTRB(11*fem, 14*fem, 11*fem, 11*fem),
            width: 163*fem,
            decoration: BoxDecoration (
              borderRadius: BorderRadius.circular(15*fem),
              gradient: const LinearGradient (
                begin: Alignment(-0.933, 1),
                end: Alignment(1, -0.873),
                colors: <Color>[Color(0xffdf5204), Color(0xfff15e02), Color(0xfffbab02)],
                stops: <double>[0, 0.524, 1],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // receipttextj6j (I4901:21698;3179:168954)
                  margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 8*fem),
                  width: 24*fem,
                  height: 24*fem,
                  child: Image.asset(
                    'assets/images/receipt-text.png',
                    width: 24*fem,
                    height: 24*fem,
                  ),
                ),
                SizedBox(
                  // autogroupgnxtqfZ (AK6DQyMRLFCoKmMNX4gNxT)
                  width: 126*fem,
                  height: 45*fem,
                  child: Stack(
                    children: [
                      Positioned(
                        // aNF (I4901:21698;3179:168950)
                        left: 0*fem,
                        top: 0*fem,
                        child: Align(
                          child: SizedBox(
                            width: 126*fem,
                            height: 24*fem,
                            child: Text(
                              'Реализация: 38 шт',
                              style: SafeGoogleFont (
                                'Open Sans',
                                fontSize: 13*ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.8461538462*ffem/fem,
                                color: const Color(0xffffffff),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        // Tgw (I4901:21698;3179:168952)
                        left: 0*fem,
                        top: 21*fem,
                        child: Align(
                          child: SizedBox(
                            width: 82*fem,
                            height: 24*fem,
                            child: Text(
                              '176 000 ₽',
                              style: SafeGoogleFont (
                                'Open Sans',
                                fontSize: 18*ffem,
                                fontWeight: FontWeight.w700,
                                height: 1.3333333333*ffem/fem,
                                color: const Color(0xfffafafa),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}