import 'package:flutter/material.dart';
import 'package:myapp/utils.dart';

class TabBarServices extends StatefulWidget {
  const TabBarServices({super.key});

  @override
  State<TabBarServices> createState() => _TabBarServicesState();
}

class _TabBarServicesState extends State<TabBarServices>
    with TickerProviderStateMixin  {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      length: 3,
      initialIndex: 0,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
    Container(
      height: 160,
      width: double.infinity,
      child: Column(children: [
        Container(
          width: double.infinity,
          height: 65,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Color(0xfffafafa),
          ),
          child:
          TabBar(
            controller: tabController,
            indicatorPadding: EdgeInsets.symmetric(vertical: 10),
            indicator: const ShapeDecoration(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  side: BorderSide(color: Colors.white)),
              color: Colors.white,
            ),
            labelColor: Color(0xff3a4060),
            labelStyle: TextStyle(color: Colors.white),
            labelPadding: const EdgeInsets.all(0),
            tabs: [
              _tab('Услуги'),
              _tab('История \nсписаний', true),
              _tab('История \nпродаж'),
            ],
          ),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: <Widget>[
                  ListView(
                    padding: const EdgeInsets.only(top: 10.0),
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(top: 2.0),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xffffffff),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xfff3f3f3),
                                offset: Offset(0, 4),
                                blurRadius: 10,
                              ),
                            ]
                        ),
                        child: Card(
                            clipBehavior: Clip.hardEdge,
                            shadowColor: Colors.transparent,
                            margin: EdgeInsets.all(15),
                            child: InkWell(
                                onTap: () {},
                                child:  Row(
                                  children: [
                                    Text(
                                      '5 шт ',
                                      style: SafeGoogleFont (
                                        'Open Sans',
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                        height: 1.72,
                                        color: Color(0xff333e63),
                                      ),
                                    ),
                                    Text(
                                      // 115 (I4901:21693;2959:167578)
                                      '1ПТ 45мин Мастер Аква',
                                      style: SafeGoogleFont (
                                        'Open Sans',
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        height: 1.72,
                                        color: Color(0xff333e63),
                                      ),
                                    ),
                                  ],
                                )
                            )
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.only(top: 2.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xffffffff),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xfff3f3f3),
                                offset: Offset(0, 4),
                                blurRadius: 10,
                              ),
                            ]
                        ),
                        child: Card(
                            clipBehavior: Clip.hardEdge,
                            shadowColor: Colors.transparent,
                            margin: EdgeInsets.all(15),
                            child: InkWell(
                                onTap: () {},
                                child:  Row(
                                  children: [
                                    Text(
                                      '4 шт ',
                                      style: SafeGoogleFont (
                                        'Open Sans',
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                        height: 1.72,
                                        color: Color(0xff333e63),
                                      ),
                                    ),
                                    Text(
                                      // 115 (I4901:21693;2959:167578)
                                      '1ПТ 55мин ТЗ',
                                      style: SafeGoogleFont (
                                        'Open Sans',
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        height: 1.72,
                                        color: Color(0xff333e63),
                                      ),
                                    ),
                                  ],
                                )
                            )
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 2.0),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xffffffff),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xfff3f3f3),
                                offset: Offset(0, 4),
                                blurRadius: 10,
                              ),
                            ]
                        ),
                        child: Card(
                            clipBehavior: Clip.hardEdge,
                            shadowColor: Colors.transparent,
                            margin: EdgeInsets.all(15),
                            child: InkWell(
                                onTap: () {},
                                child:  Row(
                                  children: [
                                    Text(
                                      '5 шт ',
                                      style: SafeGoogleFont (
                                        'Open Sans',
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                        height: 1.72,
                                        color: Color(0xff333e63),
                                      ),
                                    ),
                                    Text(
                                      // 115 (I4901:21693;2959:167578)
                                      '1ПТ 45мин Мастер Аква',
                                      style: SafeGoogleFont (
                                        'Open Sans',
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        height: 1.72,
                                        color: Color(0xff333e63),
                                      ),
                                    ),
                                  ],
                                )
                            )
                        ),
                      ),
                    ],
                  ),
                  Center(
                    child: Text("История списаний"),
                  ),
                  Center(
                    child: Text("История продаж"),
                  )
                ]
            )
        )
      ],),
    );
  }
}

Widget _tab(String text, [bool needDivider = false]) {
  final boxDecoration = BoxDecoration(border: Border(right: BorderSide(color: Color(0xff3A4060), width: 1, style: BorderStyle.solid)));
  return Container(
    padding: const EdgeInsets.all(0),
    width: double.infinity,
    decoration: needDivider ? boxDecoration : null,
    child: Tab(
      text: text,
    ),
  );
}