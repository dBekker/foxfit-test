import 'package:flutter/material.dart';
import 'package:myapp/utils.dart';

class MoreScreen extends StatelessWidget {
  const MoreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('More Screen'),
    );
  }
}