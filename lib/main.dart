import 'package:flutter/material.dart';
import 'package:myapp/screens/clients/clients.dart';
import 'package:myapp/screens/funnel/funnel.dart';
import 'package:myapp/screens/more/more.dart';
import 'package:myapp/screens/schedule/schedule.dart';
import 'package:myapp/screens/statistics/statistics.dart';
import 'package:myapp/utils.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  const MyApp({super.key});

	@override
	_MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
	int _selectedIndex = 0;

	static final List<Widget> _widgetOptions = <Widget>[
		StatisticsScreen(),
		ClientsScreen(),
		ScheduleScreen(),
		FunnelScreen(),
		MoreScreen(),
	];

	static final List<String> _titleOptions = <String>[
		"Статистика",
		"Клиенты",
		"Расписание",
		"Воронка",
		"Еще",
	];

	void _onItemTapped(int index) {
		setState(() {
			_selectedIndex = index;
		});
	}

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'FoxFit',
			debugShowCheckedModeBanner: false,
			theme: ThemeData(
					scaffoldBackgroundColor: Colors.white
			),
			routes: {
				'/statistics': (context) => StatisticsScreen(),
				'/clients': (context) => ClientsScreen(),
				'/schedule': (context) => ScheduleScreen(),
				'/funnel': (context) => FunnelScreen(),
				'/more': (context) => MoreScreen(),
			},
			home: Scaffold(
				appBar: PreferredSize(
						preferredSize: const Size.fromHeight(55.0),
						child: AppBar(
							elevation: 0,
							centerTitle: false,
							title: Text(
								_titleOptions.elementAt(_selectedIndex),
								textAlign: TextAlign.left,
								style: SafeGoogleFont(
									'Open Sans',
									fontSize: 18,
									fontWeight: FontWeight.w700,
									height: 1,
									color: const Color(0xff333e63),
								),
							),
							backgroundColor: const Color(0xfffafafa),
							actions: <Widget>[
								_Badge(
									count: 42,
									child: _IconButton(
										icon: Image.asset(
											'assets/images/ring.png',
											width: 25,
											height: 25,
										),
									),
								)

							],
						)
				),
				body: SingleChildScrollView(
							child: Column(children:[
								_widgetOptions.elementAt(_selectedIndex)
							])),
				bottomNavigationBar: SizedBox(
						height: 100,
						child: BottomNavigationBar(
							items:  <BottomNavigationBarItem>[
								BottomNavigationBarItem(
									icon: Image.asset(
										'assets/images/chart-2.png',
										width: 25,
										height: 25,
									),
									label: _titleOptions.elementAt(0),
								),
								BottomNavigationBarItem(
									icon: Image.asset(
										'assets/images/user-square.png',
										width: 25,
										height: 25,
									),
									label: _titleOptions.elementAt(1),
								),
								BottomNavigationBarItem(
									icon: Image.asset(
										'assets/images/calendar-2.png',
										width: 25,
										height: 25,
									),
									label: _titleOptions.elementAt(2),
								),
								BottomNavigationBarItem(
									icon: Image.asset(
										'assets/images/filter-tick.png',
										width: 25,
										height: 25,
									),
									label: _titleOptions.elementAt(3),
								),
								BottomNavigationBarItem(
									icon: Image.asset(
										'assets/images/group-7359.png',
										width: 25,
										height: 25,
									),
									label: _titleOptions.elementAt(4),
								),
							],
							currentIndex: _selectedIndex,
							selectedFontSize: 12,
							unselectedFontSize: 12,
							showUnselectedLabels: true,
							selectedItemColor: const Color(0xffff1e00),
							unselectedItemColor: const Color(0xff74788e),
							onTap: _onItemTapped,
						)
				),
			)
		);
	}
}

@immutable
class _IconButton extends StatelessWidget {
	final Widget icon;

	const _IconButton({
		required this.icon,
		Key? key,
	}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return IconButton(
			padding: const EdgeInsets.all(4),
			icon: icon,
			onPressed: () {},
		);
	}
}

@immutable
class _Badge extends StatelessWidget {
	final int count;
	final Widget child;

	const _Badge({
		required this.count,
		required this.child,
		Key? key,
	}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return Stack(
			children: [
				Center(child: child),
				if (count > 0)
					Positioned(
						top: 7,
						right: 7,
						child: Container(
							padding: const EdgeInsets.all(4),
							decoration: BoxDecoration(
								color: const Color(0xffff1e00),
								shape: BoxShape.circle,
								border: Border.all(color: Colors.white),
							),
							child: Text(
								'$count',
								style: const TextStyle(
									fontSize: 10,
									fontWeight: FontWeight.bold,
									color: Colors.white,
								),
							),
						),
					),
			],
		);
	}
}